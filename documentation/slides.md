---
title:
- "INF205: creating contours using the marching squares algorithm"
author:
- Trygve og Esther

---

# Output of our program:
![Contour map of Ås](kurver_ås.png)

# Marching squares
![By Nicoguaro; Adroitwhiz - File:Marching_squares_algorithm.svg, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=143418710](ms_wikipedia.svg)

# The logical flow of our program
```cpp
int main(int argc, const char* argv[])
{
    const char* filepath = argv[1];
    HeightMap map(filepath); 
    map.blur(0.8)
    auto lines = create_lines(&map, 5);
    write_output_file(cellmaps, "out.geojson", &map);
}
```

# Performance
|                    |       Time         |
|--------------------|--------------------|
|  12 threads        |     41s            |
|   1 thread         |     116s           |


# Problems we encountered

