% INF205 Lab 5
% Esther and Trygve
% April 23 2024

# 29. Glossary contributions:
## dynamic library
Dynamic libraries are libraries that are pre compiled and often provided by the operating system, as opposed to compiling them as part of your project. This saves space as each program dont need its own "copy".

## command-line arguments
Command-line arguments are extra commands that we enter after the program's executable name so that the functionality of the program changes. 


# 30. Draft slides:
See slides.pdf

# 31. Basic functionality and validation per

We ran a simple benchmark against gdal_contour as the reference implementation using `time` in the shell. My laptop has a 6 core AMD Ryzen 5 PRO 5675U running linux 6.8. The average of three tests was:
```
Ours:      33,53 s
Reference:  4,46 s
```
This is despite the reference gdal implementation beinge single threaded and our being paritally paralell. We think we will gaing a significant speedup from optimizing the part of he program drawing the contours into a file. Right now its a placeholder that just creates points, not lines.  
This innital gap in performance leaves us a lot of room to improve and it will be interesting to see what the final performance will be.  
The plan is to create a more sophisticated benchmark program in c++, but for now we just use the time command

Memory usage is also very big because of our datastructure for CellMap. It is storing essentially a line in a 2d array, so most of the items are 0. We will try to fix this by making a cell type that stores its coordinates or by getting rid of the cellmap entirely.

The source code is available at [https://gitlab.com/Trygve/contour-creator](https://gitlab.com/Trygve/contour-creator) with instructions in the readme for building and running.
If you want to view the .shp file you can use this simple webapp: [https://mapshaper.org/](https://mapshaper.org/)

# 32 Special interest functionality and responsibilities
It does not seem like we will have time to add any extra functionality.

# 33. Programming project: Progress on data structure implementation

![ER diagram](ER_diagram.svg)

We need to create destructors to call free on data in HeightMap and cells in Cells as those are arrays allocated with malloc. The default copy behaviur is fine for our program.

\newpage

# 34. Parallelization
We are using openMP because we want multiple threads using the same data and the files are small enough to fit into memory (~500mb). To scale it to multiple machines we would use mpi to distrubute seperate iamge tiles to each node. 
We haven't had time to test the performance and we have only parallelized half of what is possible. But we can see that it is using all the cores.  
The following code was used for parallelization using openMP, each thread produces one "layer" of the cellmap:
```cpp
#pragma omp parallel
    {
        std::vector<CellMap> vec_private;
        #pragma omp for
        for (int i = 1; i <= num_contours; i++) 
        {
            vec_private.push_back(produce_cellmap(heightmap, heightmap->min + interval*i));
        }
        #pragma omp critical
        vector_contours.insert(vector_contours.end(), vec_private.begin(), vec_private.end());
    }
```