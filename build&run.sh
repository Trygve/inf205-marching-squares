#! /bin/sh
rm -rf build
mkdir build
cmake -DCMAKE_BUILD_TYPE=Debug -B build
cmake --build build --parallel 
build/contour-creator --interval=1 --blur --stats example_files/ås_crop.tif