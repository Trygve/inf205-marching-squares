# Contour Creator 🗺️
This program takes a tiff heightmap and produces vector contours.
This is our project for the INF205: Resource-efficient programming course
## Status
- [x] Read .tif file into memory using gdal
- [x] Run the marching squares algorithm and produce a "cellmap"
- [x] Use a lookuptable to produce a vector file from the "cellmap"
## Dependencies
- GDAL >= 3.5
- OpenMP
- CMake

If you want to clone the repo with the example files you need [git-lfs](https://git-lfs.com/) installed and activated with ´git lfs install´  
You also need to clone with submodules to get the argh library:
```
git clone --recurse-submodules https://gitlab.com/Trygve/contour-creator
```
To install the packages on Fedora run  
```
dnf install g++ git-lfs cmake gdal-devel
```
on Debian:
```
apt install g++ git-lfs cmake libgdal-dev
```
## How to build:
```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
cmake --build . --parallel
```
Then you can run `./contour-creator PATH/TO/HEIGTHMAP.TIF`  
Run an example file: `./contour-creator "../example_files/ås.tif"`

## Docs
[Doxygen output](https://trygve.gitlab.io/contour-creator/)