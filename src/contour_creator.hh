#include <gdal/ogr_spatialref.h>
#include <gdal/ogr_spatialref.h>
#include <ogr_spatialref.h>

/**
    @brief stores the contents of a tif file with float32 values
 */
class HeightMap
{
    public:
        float* data;
        double* geotransform; //!< Six double buffer for storing the affine transformations
        // https://gdal.org/api/gdaldataset_cpp.html#_CPPv4N11GDALDataset15GetGeoTransformEPd
        int width; //!< width of image in pixels
        int height; //!< height of image in pixels
        float min; //!< Minimum value in image
        float max; //!< Maximum value in image
        const char* filepath; //!< Where the heightmap was read from
        OGRSpatialReference reference_system;
        HeightMap(const char* filepath);
        ~HeightMap()
        {
            delete[] data;
            delete[] geotransform;
        }
        float get_pixel(int x,int y);
        void blur(float standard_deviation);
        void statistics(); //!< Print statistical information about the heightmap
        void calculate_steepness(const char*); //!< Output a raster file with the steepness of each pixel
};

class Point
{
    public:
        int x;
        int y;
        uint8_t mscase; //!< The case outputted by the algorithm. A number between 0 and 15
        bool allocated = false; //!< Used when sorting the points to see if it has been sorted
        Point(int x, int y, uint8_t mscase){
            this -> x = x;
            this -> y = y;
            this -> mscase = mscase;
        };
};
